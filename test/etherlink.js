const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Test Contract", function () {

  it("Should return the new number once it's changed", async function () {

    const Etherlink = await ethers.getContractFactory("Etherlink");
    const etherlink = await Etherlink.deploy();
    await etherlink.deployed();

    expect(await etherlink.retrieve()).to.equal(0);

    const setNewNumber = await etherlink.store(2);

    // wait until the transaction is mined
    await setNewNumber.wait();

    expect(await etherlink.retrieve()).to.equal(2);
  });


});
