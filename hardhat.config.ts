import { HardhatUserConfig } from "hardhat/types";
import * as dotenv from "dotenv";

// Hardhat plugins
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-waffle";
import "hardhat-deploy";
import "solidity-coverage";
require("hardhat-contract-sizer");
require('hardhat-deploy-ethers');
require("@openzeppelin/hardhat-upgrades")

dotenv.config();

const config: HardhatUserConfig = {
  solidity: {
    compilers: [
      {
        version: "0.8.13",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
    ],
  },
  namedAccounts: {
    deployer: {
      default: 0,
    },
  },
  networks: {
    hardhat: {
      allowUnlimitedContractSize: true,
    },
    //Testnets
    etherlink: {
      url: "https://node.ghostnet.etherlink.com",
      chainId: 128123,
      accounts: [`${process.env.KEY}`],
    },
  },
};

export default config;
