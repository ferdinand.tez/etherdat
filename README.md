# Hardhat template for etherlink deployment


### Install

```
npm i 
```

### Config

create `.env` file in project root dir and put
```
KEY=YOUR_PRIVATE_KEY
```

### Deploy

For deploy contract on local network, run : 
```
npx hardhat deploy
```

For deploy contract on etherlink, run : 
```
npx hardhat deploy --network etherlink
```

### Testing

For test contract with local network, run

```
npx hardhat test test/etherlink.js
```

For test contract on etherlink, run

```
npx hardhat test test/etherlink.js --network etherlink
```

