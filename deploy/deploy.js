module.exports = async function ({ deployments, getNamedAccounts }) {
  const { deploy } = deployments
  const { deployer } = await getNamedAccounts()
  console.log(`>>> your address: ${deployer}`)
  console.log(`On [${hre.network.name}] `)

  let contract = await deploy('Etherlink', {
    from: deployer,
    args: [],
    log: true,
    waitConfirmations: 1,
  })

  console.log(`✅ Deployment on [${hre.network.name}] success`)
  console.log(`✅ Transaction hash on [${hre.network.name}]: ${contract.transactionHash}`)
  console.log(`✅ Contract address on [${hre.network.name}]: ${contract.address}`)

}

module.exports.tags = ['Etherlink'] // Tags are used to filter tags
